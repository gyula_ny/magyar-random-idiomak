"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var fonevek = require('./szavak/fonevek.json');
var melleknevek = require('./szavak/melleknevek.json');
function randomSzo(lista) {
    return lista.length === 0 ? "" : lista[Math.floor(Math.random() * lista.length)];
}
function randomMagyarKifejezes(mell, sep) {
    if (mell === void 0) { mell = 1; }
    if (sep === void 0) { sep = ' '; }
    var ret = "";
    if (mell < 2) {
        ret = "" + randomSzo(melleknevek) + sep + randomSzo(fonevek);
    }
    else {
        for (var i = 0; i < mell; i++) {
            ret = ret.concat(randomSzo(melleknevek) + sep);
        }
        ret = ret.concat(randomSzo(fonevek));
    }
    return ret;
}
// console.log(randomMagyarKifejezes(2, "-"));
exports.default = randomMagyarKifejezes;
