

const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('./index');
chai.use(chaiHttp);

const should = chai.should();

describe('a simple request to get 200', ()=> {
	it('should return 200', (done) => {
		chai.request(server)
		.get('/')
		.end((err, res) => {
			res.should.have.status(200);
			done();
		});
	});
});

describe('a random expression', ()=> {
	it('should return 200', (done) => {
		chai.request(server)
		.get('/api/v1/r/json/2')
		.end((err, res) => {
			console.log(res.text);
			res.should.have.status(200);
			done();
		});
	});
	it('should return a STRING in text', (done) => {
		chai.request(server)
		.get('/api/v1/r/json/2')
		.end((err, res) => {
			res.text.should.be.a('string');
			done();
		});
	});
	it('should have an "-" in the returned string', (done) => {
		chai.request(server)
		.get('/api/v1/r/json/2')
		.end((err, res) => {
			console.log(res.text);
			res.text.should.include('-');
			done();
		});
	});
});


describe('Bad requests', function() {
	it('incorrect api/v1 (no valid format) url should return 400', (done) => {
		chai.request(server)
		.get('/api/v1/r/lofasz/2')
		.end((err, res) => {
			console.log(res.text);
			res.should.have.status(400);
			done();
		});
	});

	it('incorrect number (of adjs) url should return 400', (done) => {
		chai.request(server)
		.get('/api/v1/r/json/5')
		.end((err, res) => {
			console.log(res.text);
			res.should.have.status(400);
			done();
		});
	});

	it('incorrect api/v1 url (no number) should return 400', (done) => {
		chai.request(server)
		.get('/api/v1/r/json')
		.end((err, res) => {
			console.log(res.text);
			res.should.have.status(400);
			done();
		});
	});

	it('incorrect url should return 200 with help', (done) => {
		chai.request(server)
		.get('/api')
		.end((err, res) => {
			console.log(res.text);
			res.should.have.status(200);
			res.text.should.include('Random magyar kifejezes API');
			done();
		});
	});
});

