"use strict";
// console.log("ello");
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = __importDefault(require("express"));
var morgan_1 = __importDefault(require("morgan"));
var randommagyar_1 = __importDefault(require("./randommagyar"));
var app = express_1.default();
app.use(morgan_1.default('combined'));
var PORT = process.env.PORT || 3000;
var v1 = express_1.default.Router();
v1.get("/r/:format/:adjnumber", function (req, res) {
    var _a = req.params, format = _a.format, adjnumber = _a.adjnumber;
    if (isNaN(Number(adjnumber)) || Number(adjnumber) > 3) {
        return res.status(400).json({ status: "hiba", error: "Hibás kérés, a kérés elvárt formátumát lásd a / URL-en. (A kért melléknevek száma max. 3 lehet" });
    }
    if (format === "json") {
        res.type('application/json');
        return res.status(200).json({
            status: "ok", result: randommagyar_1.default(Number(req.params.adjnumber), "-")
        });
    }
    else if (format === "text") {
        res.type('text/html');
        return res.status(200).send(randommagyar_1.default(Number(req.params.adjnumber), "-"));
    }
    else {
        return res.status(400).json({ status: "hiba", error: "Hibás válaszformátumot adott meg." });
    }
});
v1.get("*", function (req, res) {
    res.status(400).json({
        status: "hiba",
        error: "Hibás kérés. A kérést ebben a formátumban várom: /api/v1/(text|json)/(1|2|3..), ahol a text|json a válasz várt formátuma, a szám paraméter pedig a várt (random) melléknevek száma a (random) főnév előtt"
    });
});
app.use("/api/v1", v1);
app.get('*', function (req, res) {
    res.send("Random magyar kifejezes API. Form\u00E1tum: /api/v1/<i>(text|json)</i>/<i>(1|2|3)</i> - ahol a \"text\" <em>vagy</em> \"json\" a v\u00E1lasz v\u00E1rt form\u00E1tum\u00E1ra, a sz\u00E1m param\u00E9ter pedig a k\u00E9rt random mell\u00E9knevek sz\u00E1m\u00E1ra vonatkozik.");
});
app.listen(PORT);
exports.default = app;
