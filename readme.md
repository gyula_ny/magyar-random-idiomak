## Random Magyar Idiomák

A docker random container név generáló komponenséhez (https://github.com/moby/moby/blob/master/pkg/namesgenerator/names-generator.go) hasonló, de MAGYAR NYELVŰ random név generátor.
Közvetlenül a "petname" / "golang-petname" / "python-petname" (stb.)  (http://blog.dustinkirkland.com/2015/01/introducing-petname-libraries-for.html) magyarítása.

Sima HTML formátumban (copy-paste-hez) itt fut egy példány: http://gyzy.hu/randommagyar/

API-ként használható: http://gyzy.hu/randommagyar/api/v1/(```text```|```json```)/(```1```|```2```|```3```), ahol a text|json a válasz várt formátuma, a szám paraméter pedig a várt (random) melléknevek száma a (random) főnév előtt.




Szavak forrása: https://sourceforge.net/projects/magyarispell/files/Magyar%20Ispell/1.7beta/

