
const fonevek = require('./szavak/fonevek.json');
const melleknevek = require('./szavak/melleknevek.json');

function randomSzo(lista: string[]): string {
	return lista.length === 0 ? "" : lista[Math.floor(
		Math.random() * lista.length
	)];
}

function randomMagyarKifejezes(mell:number = 1, sep: string = ' '): string {
	let ret:string = "";
	if (mell < 2) {
		ret = `${randomSzo(melleknevek)}${sep}${randomSzo(fonevek)}`;
	} else {
		for (let i=0; i<mell; i++) {
		 	ret = ret.concat(randomSzo(melleknevek) + sep);
		}
		ret =  ret.concat(randomSzo(fonevek));
	}

	return ret;
}

// console.log(randomMagyarKifejezes(2, "-"));
export default randomMagyarKifejezes;