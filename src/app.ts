
// console.log("ello");

import express from 'express'
import morgan from 'morgan'
import randomMagyarKifejezes from './randommagyar';

const app = express();
app.use(morgan('combined'));
const PORT = process.env.PORT || 3000;
const v1 = express.Router();
v1.get("/r/:format/:adjnumber", (req, res) => {
	
	const {format, adjnumber} = req.params;
	if (isNaN(Number(adjnumber)) || Number(adjnumber) > 3) {
		return res.status(400).json(
			{status: "hiba", error: "Hibás kérés, a kérés elvárt formátumát lásd a / URL-en. (A kért melléknevek száma max. 3 lehet"}
		);
	}
	if (format === "json") {
		res.type('application/json');
		return res.status(200).json({
			status: "ok", result: randomMagyarKifejezes(Number(req.params.adjnumber), "-")
		});

	} else if (format === "text") {
		res.type('text/html');
		return res.status(200).send(randomMagyarKifejezes(Number(req.params.adjnumber), "-"));
	} else {
		return res.status(400).json({status: "hiba", error: "Hibás válaszformátumot adott meg."});
	}

})
v1.get("*", (req, res) => {
	res.status(400).json({
		status: "hiba", 
		error: "Hibás kérés. A kérést ebben a formátumban várom: /api/v1/(text|json)/(1|2|3..), ahol a text|json a válasz várt formátuma, a szám paraméter pedig a várt (random) melléknevek száma a (random) főnév előtt"
	});
})

app.use("/api/v1", v1);
app.get('/', (req, res) => {
	res.status(200);
	res.write('<html><head><title>RandomMagyar</title><meta http-equiv="Content-type" content="text/html; charset=utf-8" /></head><body><pre>')
	for (let i=0; i<10; i++) {
		res.write(randomMagyarKifejezes(2, "-") + "\n");
	}
	res.end('</pre></body></html>');
});
app.get('*', (req, res) => {
	res.send(
		`Random magyar kifejezes API. Formátum: /api/v1/<i>(text|json)</i>/<i>(1|2|3)</i> - ahol a "text" <em>vagy</em> "json" a válasz várt formátumára, a szám paraméter pedig a kért random melléknevek számára vonatkozik.`
	)
})

app.listen(PORT);


export default app

